"""
@author: Dustin Wagner
"""
import pandas as pd
import pyomo.environ as oe
import pyomo.opt as opt

## Functions
def silentremove(filename):
    import os, errno
    try:
        os.remove(filename)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occurred
            
def food_optimizer(food_data, nut_data, weight, output_file):
    # Parameters
    foods_df = pd.read_csv(food_data)
    profile_df = pd.read_csv(nut_data)
    cost_weight = weight #the weighting of cost vs taste, larger number favors cost
    serving_limit = 5 #5 = 500g or 5 units maximum per food
    #min_weight_increment = 25 #the smallest food portion will be i.e. 50 grams,
                                # increasing in 50 gram increments
    infinity = float('inf')
    
    number_foods = foods_df.shape[0]
    number_nutrients = 34
    number_categories = profile_df.shape[0] - number_nutrients
    name_foods = list(foods_df.loc[:,'Long_Desc'])
    cost_foods = list(foods_df.loc[:,'Cost'])
    taste_foods = list(foods_df.loc[:,'Taste'])
    
    #inc_factor = (min_weight_increment/100)
    
    #create dictionary of the food nutrients
    a_df = foods_df.iloc[:,6:40]
    a = {}
    for i in range(0,number_foods):
        for j in range(0,number_nutrients):
            a[(i,j)] = a_df.iloc[i,j]#*inc_factor
    
    #create dictionary of the food categories
    b_df = foods_df.iloc[:,40:66]
    b = {}
    for i in range(0,number_foods):
        for j in range(0,number_categories):
            b[(i,j)] = b_df.iloc[i,j]
                
    ## Model
    model = oe.ConcreteModel(name="Diet Problem")
    ## Foods
    model.F = oe.Set(initialize=range(number_foods))
    ## Nutrients
    model.N = oe.Set(initialize=range(number_nutrients))
    ## Categories
    model.K = oe.Set(initialize=range(number_categories))
    
    ## Amount of nutrient in each food
    model.a = oe.Param(model.F, model.N, initialize=a)
    
    ## Categories of each food
    model.b = oe.Param(model.F, model.K, initialize=b)
    
    #model.x = oe.Var(model.F, within=oe.NonNegativeReals, doc="The amount of each food")
    model.x = oe.Var(model.F, within=oe.NonNegativeIntegers, bounds=(0,serving_limit), doc="The amount of each food")
    #model.x = oe.Var(model.F, within=oe.NonNegativeReals, bounds=(0,serving_limit), doc="The amount of each food")
    
    # Balance cost and taste
    def cost_taste_rule(model):
        cost = cost_weight*sum(cost_foods[i]*model.x[i] for i in model.F)
        taste = (1-cost_weight)*(-1)*sum(taste_foods[i]*model.x[i] for i in model.F)
        return cost + taste
    model.cost = oe.Objective(rule=cost_taste_rule)
      
    # Limit nutrient consumption for each nutrient
    def nutrient_rule(model, j):
        value = sum(model.a[i,j]*model.x[i] for i in model.F)
        return profile_df.loc[j,"Min"] <= value <= profile_df.loc[j,"Max"]
    model.nutrient_limit = oe.Constraint(model.N, rule=nutrient_rule)
    
    # Limit category count
    def category_rule(model, j):
        #https://stackoverflow.com/questions/45616967/pyomo-valueerror-invalid-constraint-expression#45617490     
        if not any(model.b[i,j] for i in model.F):
            return oe.Constraint.Skip   
        value = sum(model.b[i,j]*model.x[i] for i in model.F)
        return profile_df.loc[j+number_nutrients,"Min"] <=\
                value <= profile_df.loc[j+number_nutrients,"Max"]
    model.category_limit = oe.Constraint(model.K, rule=category_rule)
    
    ## Results
    
    #model.pprint()
    opt = oe.SolverFactory("glpk")
    results = opt.solve(model)
    #model.write()
    #model.Objectives.display()
    
    if (str(results.solver.termination_condition) == 'optimal'):
        # Do something when the solution is optimal
        print("Optimal solution found")
        model.nutrient_limit.display()
            
        r_list = []
        for i in model.x:
            if model.x[i].value != 0:
                #r_list.append([model.x[i].value*inc_factor*100,'g',name_foods[i],foods_df.loc[i,'Food Group']])
                r_list.append([name_foods[i],\
                               model.x[i].value,\
                               foods_df.loc[i,'UoM'],\
                               foods_df.loc[i,'Food Group'],\
                               model.x[i].value*foods_df.loc[i,'Energy'],\
                               model.x[i].value*foods_df.loc[i,'Carbohydrate, by difference'],\
                               model.x[i].value*foods_df.loc[i,'Protein'],\
                               model.x[i].value*foods_df.loc[i,'Total lipid (fat)']])
        
        results = pd.DataFrame(r_list, columns=['Name', 'Qty', 'UoM',\
                                                'Category', 'Calories',\
                                                'Carbohydrates g','Protein g','Fat g'])
        results = results.sort_values(by='Calories', ascending=False) 
        pd.options.display.float_format = '{:.2f}'.format
        silentremove(output_file)
        results.to_csv(output_file, sep=',', index = False)
        return "optimization completed successfully"
           
    else:
        # Something else is wrong
        print("No optimal solution found")
        print("Solver Status: ",  results.solver.status)
        print("Termination Condition: ",  results.solver.termination_condition)
        return "Solver Status: "+  results.solver.status + "\nTermination Condition: " + results.solver.termination_condition


if __name__ == "__main__":
    print("food.py is being run directly")
    food_optimizer('food_database_small.csv','daily_nutrient_profile.csv',0.5,'output.csv')
else:
    print("food.py is being imported into another module")
    

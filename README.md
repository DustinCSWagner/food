# Food!

A food optimizer that uses python3, csv files, and also has a graphical interface thanks to PySimpleGUI.
Inputs and outputs using CSV.

Just *feed* in a database of foods, and a nutrient profile. 
The resulting output will give you the name of the food, unit of measure, and quantity. It will also calculate the macros for each food. 
You can use the slider to affect the weighting of cost vs taste. 

Feel free to check out the larger database in .csv and bring over what youre interested in. 


## Running 
Dependencies: Python3, Pandas, Pyomo, GLPK

To get the gui:

`python3 food_gui.py`

To run the program directly, modify `food.py` to import/output the desired .csv files, then:

`python3 food.py`


## Example Pic
![test run](https://gitlab.com/DustinCSWagner/food/raw/master/simple_gui.png)
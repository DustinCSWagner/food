"""
@author: Dustin Wagner
"""
#https://www.reddit.com/r/learnpython/comments/97wks7/python_and_gui_how_to_pyqt5_the_right_way/
#https://raw.githubusercontent.com/MikeTheWatchGuy/PySimpleGUI/master/PySimpleGUI.py

import PySimpleGUI as sg
import food

def RunFoodOptimizer():
    #sg.ChangeLookAndFeel('BrownBlue')
    #sg.SetOptions(element_padding=(5,8))

    layout_1 = [ [sg.Text('Run Food Optimizer', font=('Helvetica', 16))],
                 [sg.Text('Food Database (csv)', size=(20, 1), auto_size_text=False, justification='right'),\
                  sg.FileBrowse(target=(sg.ThisRow, 2)), sg.Input(size=(40,1), key='food_database_file', tooltip='default is foods_small.csv')],
                 [sg.Text('Nutrient Profile (csv)', size=(20, 1), auto_size_text=False, justification='right'),\
                  sg.FileBrowse(target=(sg.ThisRow, 2)), sg.Input(size=(40,1), key='profile_file', tooltip='default is daily_nutrients.csv')],
                 [sg.Text('Output (csv)', size=(20, 1), auto_size_text=False, justification='right'),\
                  sg.SaveAs(target=(sg.ThisRow, 2)), sg.Input(size=(40,1), key='output_file')],
                 [sg.Text('Parameters ' + '_'  * 100, size=(70, 1))],
                 [sg.Text('Slide to indicate percentage of Taste/Cost', size=(70, 1))],
                 [sg.Text('Cost Important', size=(15, 1), auto_size_text=False, justification='center'), \
                  sg.Slider(range=(0, 100), orientation='h', size=(25, 20), default_value=50, key='cost_slider'), \
                  sg.Text('Taste Important', size=(15, 1), auto_size_text=False, justification='center')],
                 [sg.Text('_'  * 100, size=(70, 1))],
                 [sg.Submit(), sg.Cancel()]
    ]

    window = sg.Window('Food Optimizer').Layout(layout_1)
            
    event, values = window.Read()  

    window.Close()

    print(event, values['food_database_file'], values['profile_file'], values['output_file'], values['cost_slider'])      

    if event == 'Submit':
        opt_status_msg = food.food_optimizer(values['food_database_file'],values['profile_file'],values['cost_slider'],values['output_file'])
        sg.Popup('Optimization Status', opt_status_msg)      
        
RunFoodOptimizer()




